jQuery(document).ready(function ($) {
    let diapozon = 0;
    let times = 0;
    let counting = 0;
    let valOfThis = 0;
    let arrayOld = [];
    let nowGoing = 0;
    let hardOfTest = 1;
    let checkHard = true;
    let useZero = false;
    $('.startTest').prop('disabled', true);



// voices.
    let voiceSelect = document.getElementById('voice');



// Fetch the list of voices and populate the voice options.
    function loadVoices() {
        // Fetch the available voices.
        let voices = speechSynthesis.getVoices();

        // Loop through each of the voices.
        voices.forEach(function(voice, i) {
            // Create a new option element.
            let option = document.createElement('option');

            // Set the options value and text.
            option.value = voice.name;
            option.innerHTML = voice.name;

            // Add the option to the voice selector.
            voiceSelect.appendChild(option);
        });
    }

    loadVoices();

    let changeVoice = true;

    window.speechSynthesis.onvoiceschanged = function(e) {
        if (changeVoice){
            changeVoice = false;
            loadVoices();
        }

    };


    function speak(text) {
        let msg = new SpeechSynthesisUtterance();

        if (voiceSelect.value == 'Microsoft Irina Desktop - Russian' || voiceSelect.value == 'Microsoft Zira Desktop - English (United States)'){
            msg.rate = 2.5;
        } else{
            msg.rate = 1.1;
        }



        msg.text = text;

        msg.volume = 1;

        msg.pitch = 1;

        if (voiceSelect.value) {
            msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name == voiceSelect.value; })[0];
        }

        // Queue this utterance.
        window.speechSynthesis.speak(msg);
    }

    let identifikator;

    $('.main_button').on('click', function () {
        identifikator = $(this).attr('value');
        $('.left-text').html(`Обери <br> <br> рівень`);
        $('.main_manu').fadeOut(400, function () {
            $('#' + identifikator + '').fadeIn(400);
        });
    });

    $('.return').on('click', function () {
        $('#' + identifikator + '').fadeOut(400, function () {
            $('.main_manu').fadeIn(400);
            $('.left-text').html(`Обери <br> <br> категорію`);
        });
    });


    //корень
    let sqrtCheck = 0;

    $('#sqrt .section-multi input').on('click', function () {
        sqrtCheck = $(this).attr('value');
        $('.sqrtStart').prop('disabled', false);
    });

    $('.sqrtStart').on('click', function () {
        counting = 1;
        let a =0;
        do{
            a = randomMulti(sqrtCheck * 10 - 1, 1);
        }while (Math.pow(a, 2)>sqrtCheck*10 || Math.pow(a, 2)<sqrtCheck);
        let string = '√' + Math.pow(a, 2);
        let multiply = a;
        arrayOld.push(string);
        $('.testing').css('display', 'block');
        $('#' + identifikator + '').fadeOut(400, function () {
            $('.starterPopup').fadeIn(400);
            $('.left-text').html(``);
        });
        $('.realStart').off().on('click', function () {
            $('.starterPopup').fadeOut(400, function () {
                $('.newNumber').text('3').fadeIn(400, function () {
                    $('.newNumber').fadeOut(400, function () {
                        $('.newNumber').text('2').fadeIn(400, function () {
                            $('.newNumber').fadeOut(400, function () {
                                $('.newNumber').text('1').fadeIn(400, function () {
                                    $('.newNumber').fadeOut(400, function () {
                                        $('.newNumber').text('GO!').fadeIn(400, function () {
                                            setTimeout(function () {
                                                $('.newNumber').text(string);
                                            }, 1000);
                                            setTimeout(function () {
                                                $('.newNumber').fadeOut(400, function () {
                                                    $('.result').fadeIn(400);
                                                });
                                                string = string + ' = ' + multiply;
                                                countString = string;
                                                nowSize = multiply;
                                            }, 3000);
                                        })
                                    })
                                })
                            })
                        })
                    })
                });
            });
        });
    });

    //степень
    let indexCheck = 0;
    counting = 1;

    $('#index .section-multi input').on('click', function () {
        indexCheck = $(this).attr('value');
        $('.indexStart').prop('disabled', false);
    });

    $('.indexStart').on('click', function () {
        let a = randomMulti(indexCheck * 10 - 1, indexCheck);
        let string = a + ' x ' + a;
        let multiply = Math.pow(a, 2);
        arrayOld.push(string);
        $('.testing').css('display', 'block');
        $('#' + identifikator + '').fadeOut(400, function () {
            $('.starterPopup').fadeIn(400);
            $('.left-text').html(``);
        });
        $('.realStart').off().on('click', function () {
            $('.starterPopup').fadeOut(400, function () {
                $('.newNumber').text('3').fadeIn(400, function () {
                    $('.newNumber').fadeOut(400, function () {
                        $('.newNumber').text('2').fadeIn(400, function () {
                            $('.newNumber').fadeOut(400, function () {
                                $('.newNumber').text('1').fadeIn(400, function () {
                                    $('.newNumber').fadeOut(400, function () {
                                        $('.newNumber').text('GO!').fadeIn(400, function () {
                                            setTimeout(function () {
                                                $('.newNumber').text(string);
                                            }, 1000);
                                            setTimeout(function () {
                                                $('.newNumber').fadeOut(400, function () {
                                                    $('.result').fadeIn(400);
                                                });
                                                string = string + ' = ' + multiply;
                                                countString = string;
                                                nowSize = multiply;
                                            }, 3000);
                                        })
                                    })
                                })
                            })
                        })
                    })
                });
            });
        });
    });


    //деление
    counting = 1;
    let firstCheckDiv = 0;
    let secondCheckDiv = 0;

    $('#divider .section-multi input').on('click', function () {
        let checkingClass = $(this).hasClass('firstNumber');
        if (checkingClass){
            firstCheckDiv = $(this).attr('value');
        } else {
            secondCheckDiv = $(this).attr('value');
        }
        if(firstCheckDiv !=0 && secondCheckDiv != 0 && firstCheckDiv>=secondCheckDiv){
            $('.divStart').prop('disabled', false);
        }
    });

    $('.divStart').on('click', function () {
        let a = randomMulti(firstCheckDiv * 10 - 1, firstCheckDiv);
        let b = randomMulti(secondCheckDiv * 10 - 1, secondCheckDiv);
        if (a-b<=firstCheckDiv){
            a = a+b;
        }
        a = parseInt(a/b)*b;
        let string = a + ' / ' + b;
        let multiply = a / b;
        arrayOld.push(string);
        $('.testing').css('display', 'block');
        $('#' + identifikator + '').fadeOut(400, function () {
            $('.starterPopup').fadeIn(400);
            $('.left-text').html(``);
        });
        $('.realStart').off().on('click', function () {
            $('.starterPopup').fadeOut(400, function () {
                $('.newNumber').text('3').fadeIn(400, function () {
                    $('.newNumber').fadeOut(400, function () {
                        $('.newNumber').text('2').fadeIn(400, function () {
                            $('.newNumber').fadeOut(400, function () {
                                $('.newNumber').text('1').fadeIn(400, function () {
                                    $('.newNumber').fadeOut(400, function () {
                                        $('.newNumber').text('GO!').fadeIn(400, function () {
                                            setTimeout(function () {
                                                $('.newNumber').text(string);
                                            }, 1000);
                                            setTimeout(function () {
                                                $('.newNumber').fadeOut(400, function () {
                                                    $('.result').fadeIn(400);
                                                });
                                                string = string + ' = ' + multiply;
                                                countString = string;
                                                nowSize = multiply;
                                            }, 3000);
                                        })
                                    })
                                })
                            })
                        })
                    })
                });
            });
        });
    });

    // умножение
    counting = 1;
    let firstCheck = 0;
    let secondCheck = 0;

    $('#multiplication .section-multi input').on('click', function () {
        let checkingClass = $(this).hasClass('firstNumber');
        if (checkingClass){
            firstCheck = $(this).attr('value');
        } else {
            secondCheck = $(this).attr('value');
        }
        if(firstCheck !=0 && secondCheck != 0){
            $('.multiStart').prop('disabled', false);
        }
    });


    $('.multiStart').on('click', function () {
        let a = randomMulti(firstCheck * 10 - 1, firstCheck);
        let b = randomMulti(secondCheck * 10 - 1, secondCheck);
        let string = a + ' x ' + b;
        let multiply = a * b;
        arrayOld.push(string);
        $('.testing').css('display', 'block');
        $('#' + identifikator + '').fadeOut(400, function () {
            $('.starterPopup').fadeIn(400);
            $('.left-text').html(``);
        });
        $('.realStart').off().on('click', function () {
            $('.starterPopup').fadeOut(400, function () {
                $('.newNumber').text('3').fadeIn(400, function () {
                    $('.newNumber').fadeOut(400, function () {
                        $('.newNumber').text('2').fadeIn(400, function () {
                            $('.newNumber').fadeOut(400, function () {
                                $('.newNumber').text('1').fadeIn(400, function () {
                                    $('.newNumber').fadeOut(400, function () {
                                        $('.newNumber').text('GO!').fadeIn(400, function () {
                                            setTimeout(function () {
                                                $('.newNumber').text(string);
                                            }, 1000);
                                            setTimeout(function () {
                                                $('.newNumber').fadeOut(400, function () {
                                                    $('.result').fadeIn(400);
                                                });
                                                string = string + ' = ' + multiply;
                                                countString = string;
                                                nowSize = multiply;
                                            }, 3000);
                                        })
                                    })
                                })
                            })
                        })
                    })
                });
            });
        });
    });


    let randomMulti = function (max, min) {
        let rand = 0;
        if (useZero){
            while (rand == 0) {
                rand = min - 0.5 + Math.random() * (max - min+1);
                rand = Math.round(rand);
            }
        } else{
            rand = min - 0.5 + Math.random() * (max - min+1);
            rand = Math.round(rand);
        }


        return rand;
    };
    // сложение/вычитание


    $('.hardest').on('click', function(){
        hardOfTest = $(this).val();
        $('.hardest').removeClass('active');
        $(this).addClass('active');
        if (hardOfTest > 10){
            checkHard = false;
            $('#voicesOn').prop('disabled', true);
            $('#voicesOn').prop('checked', false);
        } else {
            checkHard=true;
            if ($('#time').val()>=1){
                $('#voicesOn').prop('disabled', false);
            }
        }
    });



    $('#iteration').on('change', function(){
        if ($(this).val()<1){
            $(this).val(1);
            return false;
        }
    });
    $('#time').on('change', function(){
        if ($(this).val()<0.1){
            $(this).val(0.1);
            $('#voicesOn').prop('disabled', true);
            $('#voicesOn').prop('checked', false);
            return false;
        } else if ($(this).val()<1){
            $('#voicesOn').prop('disabled', true);
            $('#voicesOn').prop('checked', false);
        } else if (checkHard){
            $('#voicesOn').prop('disabled', false);
        }
    });


    $('.plusTime').on('click', function () {
        console.log('talk');
        let newVal = parseFloat($('#time').val())+0.1;
        $('#time').val(parseFloat(newVal).toFixed(1));
        if(newVal > 1 && checkHard){
            $('#time').val(parseFloat(newVal).toFixed(1));
            $('#voicesOn').prop('disabled', false);
        }
    });
    $('.minusTime').on('click', function () {
        let newVal = parseFloat($('#time').val())-0.1;
        if(newVal < 0.1){
            return false
        }else if(newVal < 1){
            $('#time').val(parseFloat(newVal).toFixed(1));
            $('#voicesOn').prop('disabled', true);
            $('#voicesOn').prop('checked', false);
        }else {
            $('#time').val(parseFloat(newVal).toFixed(1));
            if (checkHard){
                $('#voicesOn').prop('disabled', false);
            }
        }
    });
    $('.plusIteration').on('click', function () {
        let newVal = parseFloat($('#iteration').val())+1;
        $('#iteration').val(newVal);
    });
    $('.minusIteration').on('click', function () {
        let newVal = parseFloat($('#iteration').val())-1;
        if(newVal < 1){
            return false
        }else{
            $('#iteration').val(newVal);
        }
    });

    $('.someFriends .countOfCheck').on('change', function () {
        $('.startTest').prop('disabled', false);
        valOfThis = $(this).val();
        let arrayOfCheckbox = $('.someFriends .countOfCheck');
        let lengthOfArray = arrayOfCheckbox.length;
        for (let i = 0; i<lengthOfArray; i++){
            if (i<valOfThis-2){
                $('.someFriends .countOfCheck').eq(i).prop("checked", true);
            } else{
                $('.someFriends .countOfCheck').eq(i).prop("checked", false);
            }
        };
    });

    $('.startTest').on('click', function () {
        times = parseFloat($('#time').val());
        counting = parseInt($('#iteration').val());
        diapozon = parseInt(valOfThis);
        nowGoing = 0;
        arrayOld = [];
        useZero = false;
        $('.testing').css('display', 'block');
        $('#' + identifikator + '').fadeOut(400, function () {
            $('.starterPopup').fadeIn(400);
            $('.left-text').html(``);
        });
        $('.realStart').off().on('click', function () {
            $('.starterPopup').fadeOut(400, function () {
                $('.newNumber').text('3').fadeIn(400, function () {
                    $('.newNumber').fadeOut(400, function () {
                        $('.newNumber').text('2').fadeIn(400, function () {
                            $('.newNumber').fadeOut(400, function () {
                                $('.newNumber').text('1').fadeIn(400, function () {
                                    $('.newNumber').fadeOut(400, function () {
                                        $('.newNumber').text('GO!').fadeIn(400, function () {
                                            cicleOfCreated();
                                        })
                                    })
                                })
                            })
                        })
                    })
                });
            });
        });


    });



    let cicleOfCreated = function () {
        useZero = false;
        let timerId = setInterval(function() {
            nowGoing++;
            if (nowGoing <= counting){
                startTest();
            } else {
                $('.newNumber').fadeOut(400, function () {
                    $('.result').fadeIn(400);
                });
                countString = countString + ' = ' + nowSize;
                clearInterval(timerId);
            };
        }, times*1000);
    };
    let nowSize = 0;
    let countString = '';

    let replay = function(){
        $('.newNumber').text('3').fadeIn(400, function () {
            $('.newNumber').fadeOut(400, function () {
                $('.newNumber').text('2').fadeIn(400, function () {
                    $('.newNumber').fadeOut(400, function () {
                        $('.newNumber').text('1').fadeIn(400, function () {
                            $('.newNumber').fadeOut(400, function () {
                                $('.newNumber').text('GO!').fadeIn(400, function () {
                                    let timerId = setInterval(function() {
                                        nowGoing++;
                                        if (nowGoing <= counting){
                                            let createdNum = arrayOld[nowGoing-1];
                                            let newColor = getRandomColorHex();
                                            if (identifikator == 'addition'){
                                                $('.newNumber').css('color', newColor);
                                                if (createdNum > 0){
                                                    $('.newNumber').text('+' + createdNum);
                                                    if ($('#voicesOn').prop('checked')==true){
                                                        speak('+ ' + createdNum + '');
                                                    }
                                                }else{
                                                    $('.newNumber').text(createdNum);
                                                    if ($('#voicesOn').prop('checked')==true){
                                                        speak('' + createdNum + '');
                                                    }
                                                }
                                            }else{
                                                $('.newNumber').text(createdNum);
                                            }

                                        } else {
                                            setTimeout(function () {
                                                $('.newNumber').fadeOut(400, function () {
                                                    $('.result').fadeIn(400);
                                                });
                                            }, 1000);
                                            clearInterval(timerId);
                                        };
                                    }, times*1000);
                                })
                            })
                        })
                    })
                })
            })
        });

    };


    let startTest = function () {

        let createdNum = 0;
        let useNow = nowSize;
        let booferNow = nowSize;
        for (let i = 1; i<=hardOfTest; i*=10){
            useZero = true;
            useNow = booferNow;
            booferNow = useNow%(1000/(1000/hardOfTest*i));
            useNow = (useNow - booferNow)/(hardOfTest/i);
            let min = 0 - useNow;
            let max = diapozon - useNow;
            diapozon - useNow > diapozon ? max = diapozon : max = diapozon - useNow;
            if (createdNum<0){
                max = 0;
            } else if (createdNum>0){
                min = 0;
            }
            let stepedNum =0;
            console.log(useNow);
            if (useNow<=5){
                do{
                    stepedNum= randomGeneric(max, min);
                }while(stepedNum >= 5-useNow && stepedNum < 5)
            }else{
                do{
                    stepedNum= randomGeneric(max, min);
                }while(stepedNum < 5-useNow)
            }

            createdNum = parseInt(createdNum) + stepedNum*hardOfTest/i;
        }
        nowSize += createdNum;
        arrayOld.push(createdNum);
        let newColor = getRandomColorHex();
        $('.newNumber').css('color', newColor);
        if (createdNum > 0){
            $('.newNumber').text('+' + createdNum);
            countString = countString + '+' + createdNum;
            if ($('#voicesOn').prop('checked')==true){
                speak('+ ' + createdNum + '');
            }
        }else{
            $('.newNumber').text(createdNum);
            countString = countString + createdNum;
            if ($('#voicesOn').prop('checked')==true){
                speak('' + createdNum + '');
            }
        }


    };

    let randomGeneric = function (max, min) {
        let rand = 0;
        if (useZero){
            rand = min - 0.5 + Math.random() * (max - min+1);
            rand = Math.round(rand);
        } else{
            while (rand == 0) {
                rand = min - 0.5 + Math.random() * (max - min+1);
                rand = Math.round(rand);
            }
        }


        return rand;
    };


    $('.close').on('click', function(){
        countString = '';
        nowGoing = 0;
        nowSize = 0;
        arrayOld = [];
        $('.trueAnswer').text('');
        $('.checkMe').val('');
        $('.resultbox').fadeOut(400, function () {
            $('#' + identifikator + '').fadeIn(400);
            $('.left-text').html(`Обери <br> <br> рівень`);
        });
        $('.resultbox h3').text('');
    });


    $('.repeat').on('click', function () {
        nowGoing = 0;
        $('.checkMe').val('');
        $('.trueAnswer').text('');
        $('.resultbox').fadeOut(400, function(){
            $('.starterPopup').fadeIn(400);
            $('.left-text').html(``);
        });
        $('.realStart').off().on('click', function () {
            $('.starterPopup').fadeOut(400, function () {
                $('.newNumber').fadeIn(400);
                replay();
            });
        });
    });


    $('.continue').on('click', function () {
        $('.checkMe').val('');
        $('.trueAnswer').text('');
        $('.resultbox h3').text('');
        countString = '';
        nowGoing = 0;
        nowSize = 0;
        arrayOld = [];
        $('.resultbox').fadeOut(400, function () {
            $('.newNumber').fadeIn(400, function () {
                $('#' + identifikator + ' .newStart').click();
            });
        });

    });

    $('.backChecking').on('click', function () {
        $('.starterPopup').fadeOut(400, function () {
            $('#' + identifikator + '').fadeIn(400);
            $('.left-text').html(`Обери <br> <br> рівень`);
        });
    });

    let openPlease = true;

    $('.openTrue').on('click', function () {
        if (openPlease){
            $('.trueAnswer').text(countString).slideDown(400, function () {
                openPlease = !openPlease;
            });

        } else {
            $('.trueAnswer').slideUp(400, function () {
                openPlease = !openPlease;
            })
        }
    });

    $('.checkMe').on('keydown', function (e) {
        if (e.keyCode == 13){
            checking();
        }
    });

    $('.checkTest').on('click', function () {
        checking();
    });


    function checking() {
        $('.newNumber').text('');
        let variant = parseInt($('.checkMe').val());
        if (variant == nowSize){
            $('.resultbox h3').text('Правильно!').css('color', '#00ff00');
            $('.repeat').css('display', 'none');
        }else{
            $('.resultbox h3').text('Не правильно!').css('color', '#f00');
            $('.repeat').css('display', 'inline-block');
        };
        $('.result').fadeOut(400, function () {
            $('.resultbox').fadeIn(400);
        });
    }

    function getRandomColorHex() {
        let hex = "0123456789ABCDEF",
            color = "#";
        for (let i = 1; i <= 6; i++) {
            color += hex[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    //form in entering

    $('.enter').on('click', function (e) {
        e.preventDefault();
        window.location.pathname = 'abkus_KYCb/index.html'
    });


});